using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovPlataforma : MonoBehaviour
{
    public Animator a;
    public Animator b;
    void Start()
    {
        a.Play("Plataforma");
        b.Play("Plataforma 2");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
