using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    public Transform bulletspawn;
    public GameObject proyectil;
    public float rapidez = 1500;
    public float tirorate = 0.5f;
    private float tiempoTiro = 0;
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (Time.time > tiempoTiro)
            {
                GameObject nuevoProyectil;
                nuevoProyectil = Instantiate(proyectil, bulletspawn.position, bulletspawn.rotation);
                nuevoProyectil.GetComponent<Rigidbody>().AddForce(bulletspawn.forward * rapidez);
                tiempoTiro = Time.time + tirorate;
            }
        }
    }

}
