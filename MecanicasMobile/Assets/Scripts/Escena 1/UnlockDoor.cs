using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockDoor : MonoBehaviour
{
    public int unlockedDoors = 0;
    public int unlockedDoorsBlue = 0;
    public int unlockedDoorsGreen = 0;
    public GameObject Door1;
    public GameObject Door2;
    public GameObject Door3;
    public GameObject Door4;
    public GameObject Door5;

    public GameObject DoorB1;
    public GameObject DoorB2;
    public GameObject DoorB3;
    public GameObject DoorB4;

    public GameObject DoorG1;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Key"))
        {
            other.gameObject.SetActive(false);
            unlockedDoors++;
            unlockDoorsManager(); // orange doors
            GestorDeAudio.instancia.ReproducirSonido("KeyPicked");
        }

        if (other.gameObject.CompareTag("Key2"))
        {
            other.gameObject.SetActive(false);
            unlockedDoorsBlue++;
            unlockDoorsManager2(); // blue doors
            GestorDeAudio.instancia.ReproducirSonido("KeyPicked");
        }


        if (other.gameObject.CompareTag("Key3"))
        {
            other.gameObject.SetActive(false);
            unlockedDoorsGreen++;
            unlockDoorsManager2(); // green doors
            GestorDeAudio.instancia.ReproducirSonido("KeyPicked");
        }
    }

    private void unlockDoorsManager()
    {

        if (unlockedDoors == 1)
            Door1.SetActive(false);

        if (unlockedDoors == 4)
            Door2.SetActive(false);

        if (unlockedDoors == 6)
            Door3.SetActive(false);

        if (unlockedDoors == 11)
            Door4.SetActive(false);

        if (unlockedDoors == 16)
            Door5.SetActive(false);
    }
    private void unlockDoorsManager2()
    {
        if (unlockedDoorsBlue == 3)
            DoorB1.SetActive(false);

        if (unlockedDoorsBlue == 6)
            DoorB2.SetActive(false);

        if (unlockedDoorsBlue == 14)
            DoorB3.SetActive(false);

        if (unlockedDoorsBlue == 19)
            DoorB4.SetActive(false);

        if (unlockedDoorsGreen == 4)
            DoorG1.SetActive(false);
    }

}
