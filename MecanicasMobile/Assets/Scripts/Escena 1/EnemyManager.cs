using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public Rigidbody body;
    public GameObject SPAWN1;
    public GameObject SPAWN2;
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            MovimientoJugador.PlayerIsDown = true;

            if (LevelComplete.impar)
            {
                body.transform.position = SPAWN1.transform.position;
            }
            else
            {
                body.transform.position = SPAWN2.transform.position;
            }

        }
    }
}
