using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimations : MonoBehaviour
{
    public GameObject enemy1;
   
    void Update()
    {
        
    }

    private void LV7Enemies(bool cooldown1 = true)
    {
        if (GameManager.level == 7 && cooldown1 == true)
        {
            cooldown1 = false;
            enemy1.SetActive(false);
            new WaitForSeconds(1.35f);
            enemy1.SetActive(true);
            new WaitForSeconds(0.55f);
            enemy1.SetActive(false);
            new WaitForSeconds(1.30f);
            cooldown1 = true;
        }
    }
}
