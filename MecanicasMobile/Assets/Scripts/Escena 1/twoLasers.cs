using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class twoLasers : MonoBehaviour
{
    public GameObject l1;
    public GameObject l2;
    public bool state = true;

    private void Start()
    {
        l1.SetActive(false); l2.SetActive(false);
    }
    void Update()
    {
        if (state == true) 
            StartCoroutine(Lasers());
    }

    IEnumerator Lasers()
    {
        state = false;
        yield return new WaitForSeconds(3f);
        l1.SetActive(true);
        GestorDeAudio.instancia.ReproducirSonido("Laser");
        yield return new WaitForSeconds(0.5f);
        l2.SetActive(true);
        GestorDeAudio.instancia.ReproducirSonido("Laser");
        yield return new WaitForSeconds(3f);
        l1.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        l2.SetActive(false);
        state = true;
    }
}
