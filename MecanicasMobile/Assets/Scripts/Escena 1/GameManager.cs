using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject Wall_L;
    public GameObject Wall_R;
    public GameObject Pass_L;
    public GameObject Pass_R;
    //levels
    public GameObject lv2;
    public GameObject lv3;
    public GameObject lv4;
    public GameObject lv5;
    public GameObject lv6;
    public GameObject lv7;
    public GameObject lv8;
    public GameObject lv9;
    public GameObject lv10;
    public GameObject lv11;

    public static bool levelChange = false;
    public static int level = 1;

    void Update()
    {
        rotateWalls();
        LevelMaganer();
    }

    private void rotateWalls()
    {
        if (!levelChange)
        {
            Wall_R.SetActive(false);
            Pass_R.SetActive(true);
            Wall_L.SetActive(true);
            Pass_L.SetActive(false);
        } 
        else
        {
            Wall_R.SetActive(true);
            Pass_R.SetActive(false);
            Wall_L.SetActive(false);
            Pass_L.SetActive(true);
        }
    }

    private void LevelMaganer()
    {
        if (level == 2)
            lv2.SetActive(true);
        
        if (level == 3)
        {
            lv3.SetActive(true);
            lv2.SetActive(false);
        }
        if (level == 4)
        {
            lv3.SetActive(false);
            lv4.SetActive(true);
        }
        if (level == 5)
        {
            lv4.SetActive(false);
            lv5.SetActive(true);
        }
        if (level == 6)
        {
            lv5.SetActive(false);
            lv6.SetActive(true);
        }
        if (level == 7)
        {
            lv6.SetActive(false);
            lv7.SetActive(true);
        }
        if (level == 8)
        {
            lv7.SetActive(false);
            lv8.SetActive(true);
        }
        if (level == 9)
        {
            lv8.SetActive(false);
            lv9.SetActive(true);
        }
        if (level == 10)
        {
            lv9.SetActive(false);
            lv10.SetActive(true);
        }
        if (level == 11)
        {
            lv10.SetActive(false);
            lv11.SetActive(true);
        }

    }
}
