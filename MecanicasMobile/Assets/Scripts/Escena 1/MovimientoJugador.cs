using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoJugador : MonoBehaviour
{
    public static bool PlayerIsDown = true;
    public bool usingHability = false;
    public float velocity;
    public float UpDownForce;
    private Rigidbody rb;
    public Animator animator;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        animator.Play("CubeAnim");
    }

   
    void Update()
    {
        UpDownForceChanger();
        ChangePosY();
        Hability();
    }

    private void ChangePosY()
    {
        if (PlayerIsDown)
        {
            rb.AddForce(new Vector2(velocity, 0), ForceMode.Impulse);
            rb.AddForce(new Vector2(0,-UpDownForce), ForceMode.Impulse); 
        } 
        else 
        {
            rb.AddForce(new Vector2(velocity, 0), ForceMode.Impulse);
            rb.AddForce(new Vector2(0,UpDownForce), ForceMode.Impulse);
        }
    }

    private void UpDownForceChanger()
    {
       if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            UpDownForce = 0.2f;
            velocity = -0.1f;
            PlayerIsDown = !PlayerIsDown;
            GestorDeAudio.instancia.ReproducirSonido("Mov");
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            UpDownForce = 0.2f;
            velocity = 0.1f;
            PlayerIsDown = !PlayerIsDown;
            GestorDeAudio.instancia.ReproducirSonido("Mov");
        }
    }

    private void Hability()
    {
        if (Input.GetKeyDown(KeyCode.Mouse2))
        {
            UpDownForce = 0;
            velocity = 0;
            PlayerIsDown = !PlayerIsDown;
            if (PlayerIsDown)
            {
                rb.AddForce(new Vector2(0, -0.2f), ForceMode.Impulse);
                GestorDeAudio.instancia.ReproducirSonido("Hability");
            }
        }
    }

}
