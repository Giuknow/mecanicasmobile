using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelComplete : MonoBehaviour
{
    public static bool impar = true;
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Win"))
        {
            GestorDeAudio.instancia.ReproducirSonido("Pass");
            GameManager.levelChange = !GameManager.levelChange;
            GameManager.level++;
            impar = !impar;
        }
    }
}
