using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovColumnas : MonoBehaviour
{
    public GameObject plataforma;
    public bool iniciar = false;
  public Rigidbody body;
    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space ) && iniciar == false)
        {
            iniciar = true;
            plataforma.SetActive(false);
        }
        
        if (iniciar)
        body.AddForce(new Vector2(-0.00025f, 0), ForceMode.Impulse);
    }
}
