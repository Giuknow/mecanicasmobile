using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private Rigidbody rb;
    public float rapidez;
    public int caida;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        
    }


   
    void Update()
    {
      
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(new Vector2(0, caida), ForceMode.Impulse); ;
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Columna"))
        {
            SceneManager.LoadScene("2");
        }

    }

        public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Columna"))
        {
            SceneManager.LoadScene("2");
        }
    }
}
