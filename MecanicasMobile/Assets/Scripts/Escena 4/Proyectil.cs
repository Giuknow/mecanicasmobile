using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Proyectil : MonoBehaviour
{
    public GameObject objeto;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("proyectil"))
        {
            SceneManager.LoadScene("4");
        }
        if (collision.gameObject.CompareTag("Esfera"))
        {
            objeto.transform.SetParent(collision.transform);
            GestorDeAudio.instancia.ReproducirSonido("Door");
        }
    }
}
