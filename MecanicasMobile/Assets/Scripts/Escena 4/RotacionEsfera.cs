using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotacionEsfera : MonoBehaviour
{
    public GameObject B;
    public GameObject W;
    public bool activar = true;
    public bool Z = true;
    public bool Y = false;
    public bool X = false;
     void Update()
    {
        if (activar == true)
            StartCoroutine(rotaciones());

        if (Z == true)
            RotacionZ();

        if (Y == true)
            RotacionY();

    }

    private void RotacionZ()
    {
        transform.Rotate(new Vector3(0, 0, 45) * Time.deltaTime);
    }
    private void RotacionY()
    {
        transform.Rotate(new Vector3(0, 45, 0) * Time.deltaTime);
    }


    IEnumerator rotaciones()
    {
        activar = false;
        Z = true;
        B.SetActive(true);
        W.SetActive(false);
        GestorDeAudio.instancia.PausarSonido("Y");
        GestorDeAudio.instancia.ReproducirSonido("Z");
        yield return new WaitForSeconds(20f);
        Z = false;
        W.SetActive(true);
        B.SetActive(false);
        GestorDeAudio.instancia.ReproducirSonido("Y");
        GestorDeAudio.instancia.PausarSonido("Z");
        Y = true;
        yield return new WaitForSeconds(15f);
        Y = false;
        activar = true;
    }
}
