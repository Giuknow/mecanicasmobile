using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    public bool act = true;
    public bool actboss = true;
    public GameObject LaserTravelingenemy;
    public GameObject LaserBoss;
    public Animator TravLaser;
    public Animator BossAnim;
    void Start()
    {
        LaserTravelingenemy.SetActive(false);
        GestorDeAudio.instancia.ReproducirSonido("MainMsc");

    }

    private void Update()
    {
        AnimarLaserTraveling();
    }

    private void AnimarLaserTraveling()
    {
        if (GameManager.level == 10)
        {
            StartCoroutine(LaserTravelingShow());
            TravLaser.Play("LaserTraveling");
        }
        if (GameManager.level == 11)
        {
            StartCoroutine(Boss());
            BossAnim.Play("LaserBoss");
        }

    }

    IEnumerator LaserTravelingShow()
    {
        if (act)
        {
            act = false;
            yield return new WaitForSeconds(5.5f);
            GestorDeAudio.instancia.ReproducirSonido("Laser");
            LaserTravelingenemy.SetActive(true);
            yield return new WaitForSeconds(3f);
            LaserTravelingenemy.SetActive(false);
            yield return new WaitForSeconds(1.5f);
            act = true;
        }
    }
     IEnumerator Boss()
        {
            if (actboss)
            {
                actboss = false;
                yield return new WaitForSeconds(1.5f);
                GestorDeAudio.instancia.ReproducirSonido("Laser");
                LaserBoss.SetActive(true);
                yield return new WaitForSeconds(0.5f);
                LaserBoss.SetActive(false);
                yield return new WaitForSeconds(0.5f);
                actboss = true;
            }
        }

       
    }
