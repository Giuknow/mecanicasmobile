using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ganar : MonoBehaviour
{
    public TMPro.TMP_Text TextGanaste;
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ganar"))
        {
            TextGanaste.text = "Ganaste!";
        }
    }
}
